---
title: SSH Jump from one server to another
date: 2020-02-08 14:09:17
tags:
- linux
- jumphost
---

A jump host (also known as a jump server) is an intermediary host to a remote network, through which a connection can be made to another host. It bridges two dissimilar security zones and offers controlled access between them.

A classic scenario is connecting from your desktop or laptop from inside your company’s internal network, which is highly secured with firewalls. In order to easily manage a server in a DMZ (demilitarized zone), you may access it via a jump host.

In this scenario, you want to connect to HOST 2, but you have to go through HOST 1, because of firewalling, routing and access privileges. There is a number of valid reasons why jumphosts are needed.

## Static Jumphost List
Static jumphost list means, that you know the jumphost or jumphosts that you need to connect a machine. Therefore you need to add the following static jumphost ‘routing’ in ~/.ssh/config file and specify the host aliases as shown.


```shell
### First jumphost. Directly reachable
Host one
	Hostname example1.com
	IdentityFile ~/ssh.pem
	User bob-mcguire

### Host to jump to via jumphost1.example.org
Host two
	Hostname example2.com
	IdentityFile ~/ssh.pem
	User bob-mcguire
	ProxyJump two
```

## Now try to connect to a target server via a jump host
```shell
$ ssh -J one two
```

The above command will first open an ssh connection to server one in the background and there after, start the ssh session to the target server two.
