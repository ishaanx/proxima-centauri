---
title: AWS S3 CORS Policy For Public Access
date: 2020-01-18 06:24:16
tags: 
- aws
- cloud
category: cloud
---

A simple CORS policy for S3 bucket to access any file from external sources. I use this policy for my images hosted on https://travel.ishaanx.com

```xml
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
<CORSRule>
    <AllowedOrigin>*.example.com</AllowedOrigin>
    <AllowedMethod>HEAD</AllowedMethod>
    <AllowedMethod>GET</AllowedMethod>
    <MaxAgeSeconds>3000</MaxAgeSeconds>
    <ExposeHeader>x-amz-server-side-encryption</ExposeHeader>
    <ExposeHeader>x-amz-request-id</ExposeHeader>
    <ExposeHeader>x-amz-id-2</ExposeHeader>
    <ExposeHeader>Access-Control-Allow-Origin</ExposeHeader>
    <ExposeHeader>Access-Control-Allow-Methods</ExposeHeader>
    <AllowedHeader>*</AllowedHeader>
</CORSRule>
</CORSConfiguration>
```

## Use cases

- To create your own CDN for files such as public images, .CSS, .JS, .JSON.
