---
title: the missing guide to hexo-custom-fonts
date: 2020-01-23 23:58:22
tags: 
- hexo
- webfonts 
- fonts 
- stylus
---


## Webfonts
By using a specific CSS @font-face embedding technique it is possible to embed fonts such that they work with IE4+, Firefox 3.5+, Safari 3.1+, Opera 10+ and Chrome 4.0+. This allows the vast majority of Web users to access this functionality. 


Fonts come in various formats, with OpenType arguably the most common one for desktop use. For the web, you need web fonts, which come different file formats. FontSquirrel offers a service to generate web fonts from other font formats. They will also generate the css font-face declaration that goes with the web font they generated. 

### Add Webfonts to the Hexo Theme Source
For the website to render the content in the selected fonts, the web fonts have to be moved to fonts directory. This is accomplished by storing the web font files (.woff2, .woff) in the source/fonts directory. 

```bash
.
├── _config.yml
├── package.json
├── scaffolds
├── source
|   ├── _drafts
|   └── _posts
└── themes
|   ├── source
|       └── fonts
```

### Stylus Declaration

For Hexo themes, we can declare this codeblock in the font.styl or any stylus file that your theme is using. Although a good practice is to have a separate font.styl file to maintain proper code visibility.

```stylus
@font-face {
    font-family: 'mulilight';
    src: url('../fonts/muli/muli-light-webfont.woff2') format('woff2'),
         url('../fonts/muli/muli-light-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;
}
```

### Font Variable

Now we can create a font variable in Stylus and reference that variable wherever required.

```css
$font-family = 'mulilight'
```

