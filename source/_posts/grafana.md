---
title: Grafana and Prometheus Node Exporter
date: 2019-09-07
categories:
- linux
tags:
 - Grafana
 - Prometheus
 - Dashboard
metaAlignment: center
keywords:
- administration
- tech
- learn
- linux
- ubuntu
- server
- media
- technology
- ishan
- badgainya
- GitHub
- GitLab
- netlify
- website
- repos
- grafana
- prometheus
- nodejs
---
Server dashboards never looked this cool!
<!--more-->

There is a long list of reasons for adopting the Grafana dashboards.
Grafana dashboards are great instruments for obtaining insight into information from time series data. Furthermore, Grafana can easily integrate sensors, system and network metrics with InfluxDB and Telegraf to facilitate and provide much better information.

It's simple to set up and integrate a Grafana dashboard with different information sources. Grafana template variables allow you to generate dynamic dashboards for real-time modifications.

For someone who wants to monitor their local servers, grafana needs additional services attached, say Prometheus which I have integrated with my own home server. 
Prometheus + Grafana is a common combination of tools to build up a monitoring system.

Here is a gallery of my current dashboard - 


![Home Dashboard](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/grafana/dash.png)


## Under the Hood of Prometheus
Soundcloud developed Prometheus after migrating its operations over to Kubernetes. It is written in Go, with no external dependencies, as a static binary.

![Prometheus Concept](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/grafana/Prometheus.png)
Credits: Prometheus.io

Flashbacks of 8085 microprocessor, anyone?

Prometheus allows users to query, log, and analyze data, but not in the way traditional analysis works. By identifying streams of data as key-value pairs, Prometheus aggregates and filters specified metrics while allowing for finely-grained querying to take place. Prometheus has a flexible query language allows querying and graphing this data.
