---
title: Running Hugo server on multiple devices
date: 2018-03-14
categories:
- linux
tags:
 - linux
 - hugo
metaAlignment: center
keywords:
- tech
- learn
- linux
- ubuntu
- server
- media
- technology
- ishan
- badgainya
cover:  /hugobind/hugo_cover.jpg
description: Test your local Hugo website on phones
---

When you run Hugo server it only binds to the localhost of the machine your are running on. Let's say you want to view your current website on multiple device in your network. 
<!--more-->


By visiting your computer's IP address, it should work. Sadly, for me it didn't. So I digged the web looking for possible solutions and found this one. 

The usual way of running Hugo server is by giving the following command

```bash
hugo server -w
```

Instead bind it to your computer's IP like this

```bash
hugo server --bind=10.0.0.5 --baseURL=http://10.0.0.5:1313
```

10.0.0.5 is my computer's IP address. Make sure you open 1313 port in firewall as well.
It's a nifty litle solution and work flawlessly. 
Now you can view your live site on every device inside your LAN. 