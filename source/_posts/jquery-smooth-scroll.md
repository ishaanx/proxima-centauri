---
title: Easiest JQuery smooth scroll
date: 2020-01-21 03:58:21
tags:
- jquery
- javascript
- html
- hexo
- smoothscroll
keywords:
- jquery
- javascript
- html
- hexo
- smoothscroll
- smooth
- scroll
- css
- go_bottom
- go_top
---

The most minimalist way to inlude smooth scroll in JQuery after an `on_click` event is triggered. I specifically integrated this in my website. 

JQuery
---

```js
function go_top() {
window.scrollTo({ 
    top: 0, 
    behavior: 'smooth' });   
}
function go_bottom() {
window.scrollTo({ 
    top: document.body.scrollHeight, 
    behavior: 'smooth' }); 
}
```

HTML
---

```html
<a onclick="go_top()">Back to top</a>
<a onclick="go_bottom()">Go to bottom</a>
```    

See a demo here
---

{% oembed https://codepen.io/albusd/pen/rNaRaOr %}