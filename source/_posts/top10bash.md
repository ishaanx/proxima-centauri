---
title: Top 10 Commands That You Use On Your Command-line Terminal
date: 2020-01-01 18:46:41
tags:
- linux
- bash
---


This bash command can be used to list out your top ten most used commands based on your bash history. 

```bash
history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n10
```

Here are the results from my new personal machine:


```bash
147  31.0127%   hexo
76   16.0338%   ls
55   11.6034%   cd
37   7.80591%   npm
25   5.27426%   hugo
23   4.85232%   git
17   3.5865%    mysql
12   2.53165%   ssh
12   2.53165%   fish
11   2.32068%   brew
```


For the sake of sample size, I recommend having a large history file size, e.g for bash, put in your ~/.bashrc:

```bash
HISTSIZE=10000 #max number of commands to remember per ongoing session
HISTFILESIZE=20000 #max number of lines contained in the history file
```

Or you can use negative values to set the size to infinite.




