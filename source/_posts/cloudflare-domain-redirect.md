---
title: cloudflare domain redirect
date: 2020-02-02 16:08:05
tags:
- cloudflare
- cloud
- domain
---


I have been using my old domain for almost 3 years now and I decided to move to a new domain. My old domain - [ishanbadgainya.com][old-domain] was long, difficult to read and was poor performing with SEO. So I chose a new shorter sounding domain name - [ishaanx.com][new-domain]

The real problem here was how to remove my old domain from every search crawler out there and move existing traffic to the new domain. I have been using [Cloudflare][cloudflare-dash] since the beginning of era and it made the solution simple enough.


Redirecting domains is very easy in [Cloudflare][cloudflare-dash]. Let's say you have two domains
- https://domain1.com (Old domain)
- https://domain2.com (New domain)

And you want to redirect all traffic from domain1  to domain2, including the traffic of subpages. 
For ex:

```bash
domain1.com > domain2.com
domain1.com/page1 > domain2.com/page1
domain1.com/page2 > domain2.com/page2
abc.domain1.com > abc.domain2.com
```

{% image data-fancybox img-center https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2020/cloudflare/page_rule.png 75% Page Rules %}

Read more about Page Rules on [Cloudflare docs webpage.][cloudflare-doc]

---
[cloudflare-dash]: https://dash.cloudflare.com/login
[old-domain]: https://ishanbadgainya.com 
[new-domain]: https://ishaanx.com
[cloudflare-doc]: https://support.cloudflare.com/hc/en-us/articles/218411427-Understanding-and-Configuring-Cloudflare-Page-Rules-Page-Rules-Tutorial-



