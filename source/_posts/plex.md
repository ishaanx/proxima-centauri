---
title: Installing Plex on Ubuntu 16.04
subtitle: The main course...
date: 2017-07-26 05:34:11
tags: ["linux", "plex", "ubuntu"]
categories:
- linux
keywords:
- tech
- learn
- linux
- ubuntu
- server
- media
- technology
- ishan
- badgainya
---

Plex is a feature-rich media library platform that allows you to organize and stream your digital video and audio from virtually anywhere. This guide will show you how to set up the Plex Media Server on your Ubuntu 16.04 LTS.
<!--more-->
And  how to connect to your media server from a Plex client application. 

To start, I will assume you understand how to use command line and have a bit of background knowledge of Ubuntu or Debian based operating systems. This install isn’t terribly difficult, but you should know how to install and update applications through terminal.

---
## Plex Installation

Get the latest .deb package using `wget`:



```bash
mkdir plextmp
cd plextmp
wget https://downloads.plex.tv/plex-media-server/1.7.5.4035-313f93718/plexmediaserver_1.7.5.4035-313f93718_amd64.deb
sudo dpkg -i plexmediaserver*.deb
``` 
This package link is outdated. Please make sure that you get the latest package from Plex website. 

![Login](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/plex/Screenshot+(89).png)


### Start the Plex service


```bash
sudo systemctl enable plexmediaserver.service
sudo systemctl start plexmediaserver.service
```

### Open Ports


Plex runs on the port 32400. 
Open the port using the following command (if you are using iptables):

```bash
sudo iptables -I INPUT -p tcp --dport 32400 -j ACCEPT
```

---

### Setting up PleX

Open the following link in your browser: 


```
http://ip-address:32400/web
```

or


```
https://ip-address:32400/web
``` 

You need a PleX account to continue. So go ahead and Sign Up. 

![Login](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/plex/login.png)


Give your server any name you like.

![Login](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/plex/1.png)


Add folders where your media files are stored on your server.

![Login](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/plex/3.png)



![Login](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/plex/4.png)

Select next and you are done.


![Login](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/plex/5.png)



![Login](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/plex/6.png)

What's Next?

Once the Wizard finishes, the Server will begin scanning your media and creating the Plex Library It can take a little while to completely scan a new Library, depending on how many shows you have. You should also expect some Internet usage while metadata is fetched. You can watch media on a Plex Web App while the Library is building but it's generally better to let the process complete before using the Server.


For more information you can follow the official PleX documents.
<https://support.plex.tv/hc/en-us/sections/200225647>

There is an official standalone client for Windows. So you don't need to login using browser every time.


![Login](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/plex/7.png)

Available on [Plex's official site].
[Plex's official site]: https://www.plex.tv/downloads/

---
