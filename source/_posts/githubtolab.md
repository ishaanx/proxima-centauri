---
title: GitHub to GitLab at last
date: 2019-03-31
categories:
- linux
tags:
 - GitHub
 - GitLab
 - netlify
metaAlignment: center
keywords:
- administration
- tech
- learn
- linux
- ubuntu
- server
- media
- technology
- ishan
- badgainya
- GitHub
- GitLab
- netlify
- website
- repos


---
Migrating to GitLab
<!--more-->

A long overdue project, involves migrating all my repos from GitHub to GitLab. This migration was initially planned when Microsoft bought GitHub for $7.5 billion. Partly because GitLab offers unlimited private repos; the other reason is, I started using GitLab way before GitHub.

So the actual migration is quite a piece of cake. You head over to GitLab, sign up for a free account.

- You will see this New Project button right in your projects section.

![GitLab](https://ishan-post-photos.s3.ap-south-1.amazonaws.com/2019/githubtolab/git1.png)


- Select it and find the Import Project section.

- Import project from GitHub - Authorize your GitHub account and select the repositories you want to import.

- That's all there is to it. Everything else functions the way it used to be. Even the UI functionality is almost similar, so there is this GitHub-ie feeling all along.

The UI won't matter much if one uses command line to manage their code and repositories.

GitLab has gone all the way to support all your favorite plugins and third party apps, for example - Netlify. This static generator has the perfect integration with GitLab. 
