---
title: About
date: 2020-01-16 10:38:45
---

I am an Electronics Engineer working for an IT company with a passion for anything that runs on less than 5 volts. I like to learn new things and this website is an attempted representation of what goes inside my mind all the time. 

I started working on this website way back in 2011 during my first year of Diploma. I never published it untill 2017 when I was in my final years of graduation. This website has seen some rough days. Following is the order of hosting platforms that I have moved the website to over the years - 

- Local Network for almost 5 years
- Digital Ocean
- A 3rd gen RaspberryPi
- Amazon Web Services
- GitHub pages
- Google Cloud Platform
- GItlab + Netlify CI/CD integration (Current Setup)

This is a static website built with <a href="https://hexo.io" target="blank">Hexo</a> and a heavily modified theme called  <a href="https://github.com/Siricee/hexo-theme-Chic" target="blank">Chic</a>. I am also using <a href="https://www.heroku.com/" target="blank">Heroku</a> to serve my <a href="https://travel.ishaanx.com" target="blank">travel photos page.</a> It's a Node.js application hosted on a subdomain. SSL certificate is provide via Cloudflare and also protect against DDOS attacks.

You can connect with me on LinkedIn, Instagram, Contacts Page, Keybase. 

There is a hidden easter egg in this website. If you find it you can have the egg.

