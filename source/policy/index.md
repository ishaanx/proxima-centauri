---
title: Policies and Disclaimer
---

## Disclaimer
### No Warranties
This website is provided “as is” without any representations or warranties, express or implied. I (Ottmar Klaas) make no representations or warranties in relation to this website or the information and materials provided on this website.

Without prejudice to the generality of the foregoing paragraph, I do not warrant that:

this website will be constantly available, or available at all; or
the information on this website is complete, true, accurate or non-misleading.
Nothing on this website constitutes, or is meant to constitute, advice of any kind.

### Limitations of Liability
I will not be liable to you (whether under the law of contract, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:

to the extent that the website is provided free-of-charge, for any direct loss;
for any indirect, special or consequential loss; or
for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.
These limitations of liability apply even if I have been expressly advised of the potential loss.

### Exceptions
Nothing in this website disclaimer will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or limit my liability in respect of any:

death or personal injury caused by my negligence;
fraud or fraudulent misrepresentation on the part of me; or
matter which it would be illegal or unlawful for me to exclude or limit, or to attempt or purport to exclude or limit, its liability.

### Reasonableness
By using this website, you agree that the exclusions and limitations of liability set out in this website disclaimer are reasonable.
If you do not think they are reasonable, you must not use this website.

### Other Parties
You agree that the limitations of warranties and liability set out in this website disclaimer will protect my officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as me (Ottmar Klaas).

### Unenforceable Provisions
If any provision of this website disclaimer is, or is found to be, unenforceable under applicable law, that will not affect the enforceability of the other provisions of this website disclaimer.

## Privacy Policy
### Information that is collected
I use a tool called “Google Analytics” to collect information about use of this site. Google Analytics collects information such as how often users visit this site, what pages they visit when they do so, and what other sites they used prior to coming to this site. I use the information to help me understand what visitors like to read and how they interact with the site. Google Analytics collects only the IP address assigned to you on the date you visit this site, rather than your name or other identifying information. I do not combine the information collected through the use of Google Analytics with personally identifiable information. Although Google Analytics plants a permanent cookie on your web browser to identify you as a unique user the next time you visit this site, the cookie cannot be used by anyone but Google. Google’s ability to use and share information collected by Google Analytics about your visits to this site is restricted by the Google Analytics Terms of Use and the Google Privacy Policy. You can prevent Google Analytics from recognizing you on return visits to this site by disabling cookies on your browser or installing an ad blocker, e.g., uBlock Origin.

## Linking Policy
Status of linking policy
I welcome links to this website made in accordance with the terms of this linking policy. By using this website you agree to be bound by the terms and conditions of this linking policy.

### Links to this website
You must follow the following rules to link to this site:

### Links pointing to this website should not be misleading.
Appropriate link text should be always be used.
You must not use the my logo to link to this website (or otherwise) without my express written permission.
You must not link to this website using any inline linking technique.
You must not frame the content of this website or use any similar technology in relation to the content of this website.

### Links from this website
This website includes links to other websites owned and operated by third parties. These links are not endorsements or recommendations.

I have no control over the contents of third party websites, and I accept no responsibility for them or for any loss or damage that may arise from your use of them.