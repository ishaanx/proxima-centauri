(function() {


    var ie = !!(window.attachEvent && !window.opera);
    var wk = /webkit\/(\d+)/i.test(navigator.userAgent) && (RegExp.$1 < 525);
    var fn = [];
    var run = function() {
        for (var i = 0; i < fn.length; i++) fn[i]();
    };
    var d = document;
    d.ready = function(f) {
        if (!ie && !wk && d.addEventListener)
            return d.addEventListener('DOMContentLoaded', f, !1);
        if (fn.push(f) > 1) return;
        if (ie)(function() {
            try {
                d.documentElement.doScroll('left');
                run()
            } catch (err) {
                setTimeout(arguments.callee, 0)
            }
        })();
        else if (wk)
            var t = setInterval(function() {
                if (/^(loaded|complete)$/.test(d.readyState))
                    clearInterval(t), run()
            }, 0)
    }
})();
document.ready(function() {
    preloaderFadeOutTime = 500;

    function hidePreloader() {
        var preloader = $('.spinner-wrapper');
        preloader.delay(400).fadeOut(preloaderFadeOutTime)
    }
    hidePreloader();
    var _Blog = window._Blog || {};
    const currentTheme = window.localStorage && window.localStorage.getItem('theme');
    const isDark = currentTheme === 'dark';
    if (isDark) {
        document.getElementById("toggle--daynight").checked = !0;
        document.getElementById("mobile-toggle-theme").innerText = "Dark"
    } else {
        document.getElementById("toggle--daynight").checked = !1;
        document.getElementById("mobile-toggle-theme").innerText = "Dark"
    }
    _Blog.toggleTheme = function() {
        if (isDark) {
            document.getElementsByTagName('body')[0].classList.add('dark-theme');
            document.getElementById("mobile-toggle-theme").innerText = "Dark"
        } else {
            document.getElementsByTagName('body')[0].classList.remove('dark-theme');
            document.getElementById("mobile-toggle-theme").innerText = "Light"
        }
        document.getElementsByClassName('toggle--btn')[0].addEventListener('click', () => {
            if (document.getElementsByTagName('body')[0].classList.contains('dark-theme')) {
                document.getElementsByTagName('body')[0].classList.remove('dark-theme')
            } else {
                document.getElementsByTagName('body')[0].classList.add('dark-theme')
            }
            window.localStorage && window.localStorage.setItem('theme', document.body.classList.contains('dark-theme') ? 'dark' : 'light', )
        })
        document.getElementById('mobile-toggle-theme').addEventListener('click', () => {
            if (document.getElementsByTagName('body')[0].classList.contains('dark-theme')) {
                document.getElementsByTagName('body')[0].classList.remove('dark-theme');
                document.getElementById("mobile-toggle-theme").innerText = "Light"
            } else {
                document.getElementsByTagName('body')[0].classList.add('dark-theme');
                document.getElementById("mobile-toggle-theme").innerText = "Dark"
            }
            window.localStorage && window.localStorage.setItem('theme', document.body.classList.contains('dark-theme') ? 'dark' : 'light', )
        })
    };
    _Blog.toggleTheme()
});
window.onload = function() {
    Particles.init({
        selector: '.background',
        maxParticles: 50,
        connectParticles: !0,
        color: '#b6b6b6',
        responsive: [{
            breakpoint: 768,
            options: {
                maxParticles: 30,
                color: '#b6b6b6',
                connectParticles: !0,
                selector: '.background'
            }
        }, {
            breakpoint: 425,
            options: {
                maxParticles: 20,
                color: '#b6b6b6',
                connectParticles: !1,
                selector: '.background'
            }
        }, {
            breakpoint: 320,
            options: {
                maxParticles: 10
            }
        }]
    })
}